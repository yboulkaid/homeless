class ListingParser
  def initialize(raw_dom)
    @raw_dom = raw_dom
  end

  def title
    title_and_source.children[0].text
  end

  def source
    apartment.css('.k').children[2]
  end

  def price
    apartment.css('.j').children.first.text
  end

  def size
    apartment.css('.l').children.first.text
  end

  def link
    apartment.css('.p').children.first.first[1]
  end

  def date_added
    apartment.css('.p').children.first.text
  end

  def persist
    return if Listing.exists?(link: link)
    Listing.create(title: title, source: source, price: price, link: link, size: size,
                   date_added: date_added)
  end

  private

  attr_reader :raw_dom

  def apartment
    raw_dom
  end

  def title_and_source
    apartment.css('.k')
  end
end
