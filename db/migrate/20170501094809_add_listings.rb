class AddListings < ActiveRecord::Migration

  def change
    create_table :listings do |t|
      t.string :title
      t.string :source
      t.string :price
      t.string :size
      t.string :link
      t.string :date_added
      t.timestamp :notified_at

      t.timestamps
    end
  end

end
