class Crawler
  def initialize(query: 'södermalm')
    @query = query
    parse
  end

  def persist_listings
    listing_parsers.each_with_index do |listing_parser, i|
      next if i > 20
      listing_parser.persist
    end
  end

  private

  def listing_parsers
    @listing_parsers ||= parsed_page.css('.o .m').map { |raw_string| ListingParser.new(raw_string) }
  end

  def page_source
    @page_source
  end

  def parsed_page
    @parsed_page ||= Nokogiri::HTML page_source
  end

  def parse
    browser = Selenium::WebDriver.for :chrome
    p 'Navigating to kvalster...'
    browser.get 'http://kvalster.se'
    search_bar = browser.find_element(:id, 'c')
    p "going to #{@query}"
    search_bar.send_keys @query
    sleep 1
    search_bar.send_keys :enter
    sleep 1
    p 'Filtering...'
    browser.get(browser.current_url + '?Rum=3&maxHyra=21000&minYta=50&maxListad=5')
    # browser.get(browser.current_url + '?maxHyra=12100&maxListad=5')
    p 'Parsing...'
    raw_results = browser.find_element(class: 'o').text
    @page_source = browser.page_source
    browser.close
    raw_results
  end
end
