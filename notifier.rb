class Notifier
  def self.send_new_emails
    listings_to_send = Listing.where(notified_at: nil)
    p "Sending #{listings_to_send.count} listings"
    return unless listings_to_send.count > 0
    new(body: listings_to_send.map(&:to_html).join('')).send_email
    listings_to_send.update(notified_at: Time.now)
  end

  def initialize(body:)
    @body = body
  end

  def send_email
    sg = SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
    from = Email.new(email: 'youssef@ruby.com')
    subject = 'Kvalster update'
    to = Email.new(email: 'irenatoljan@gmail.com', name: 'Irena')
    content = Content.new(type: 'text/html', value: @body)
    mail = Mail.new(from, subject, to, content)

    # personalization = Personalization.new
    # personalization.to = Email.new(email: 'yboulkaid@gmail.com', name: 'Youyou')
    # personalization.cc = Email.new(email: 'vaprica@rambler.ru', name: 'Crazy cat lady')
    # mail.personalizations = personalization

    sg.client.mail._('send').post(request_body: mail.to_json)
  end
end
