require 'selenium-webdriver'
require 'nokogiri'
require 'sendgrid-ruby'
require 'pry'
require 'active_record'
require_relative 'crawler'
require_relative 'notifier'
require_relative 'listing'
require_relative 'listing_parser'
include SendGrid

db_config = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(db_config)

def locations
  return ARGV unless ARGV.blank?
  ['södermalm']
end

# binding.pry
if ENV['INTERACTIVE'] == 'true'
  binding.pry
else
  locations.each do |location|
    begin
      Crawler.new(query: location).persist_listings
    rescue Net::ReadTimeout
      puts "Crawling failed fo #{location}, moving on"
    end
  end
end

Notifier.send_new_emails
