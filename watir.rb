require 'selenium-webdriver'

browser = Selenium::WebDriver.for :chrome
p 'Navigating to kvalster...'
browser.get 'http://kvalster.se'
search_bar = browser.find_element(:id, 'c')
p 'Going to the right area...'
search_bar.send_keys 'södermalm'
sleep 1
search_bar.send_keys :enter
sleep 1
p 'Filtering...'
browser.get(browser.current_url + '?maxHyra=10100&maxListad=5')
p 'Parsing...'
raw_results = browser.find_element(class: 'o').text
apartments = raw_results.split(/\n/).each_slice(7).to_a
p "there are #{apartments.count} apartments available :"
apartments.each do |apartment|
  p apartment
end

require 'nokogiri'
parsed_page = Nokogiri::HTML browser.page_source
apartments = parsed_page.css('.o .m')

# apartment = apartments.first


  apartments.each do |apartment|


  title_and_source = apartment.css('.k')
  title = title_and_source.children[0].text
  source = apartment.css('.k').children[2]

  price = apartment.css('.j').children.first.text

  size = apartment.css('.l').children.first.text

  link = apartment.css('.p').children.first.first[1]
  date = apartment.css('.p').children.first.inner_html

  p "Found : #{title} (from #{source}) : #{price} - #{size} - #{date} : #{link}"
end

p 'Done, closing in 4...'
sleep 4
browser.close
# apartments.first.children.count
